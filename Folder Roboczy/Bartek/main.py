import json
import socket
import sys
import time
import numpySource as np
import matplotlib.pyplot as plt
import math


class NoobBot(object):

    #json API
    global race, track, pieces, lanes, last

    def get_piece_index(self,data):
        if(len(data) == 1):
            data = data[0]
        return data['piecePosition']['pieceIndex']

    def get_in_piece_Distance(self,data):
        if(len(data) == 1):
            data = data[0]
        return data['piecePosition']['inPieceDistance']

    def get_car_angle(self,data):
        if(len(data) == 1):
            data = data[0]
        return data['angle']

    def get_start_lane_index(self,data):

        if(len(data) == 1):
            data = data[0]
        return data['piecePosition']['lane']['startLaneIndex']

    def get_end_lane_index(self,data):
       if(len(data) == 1):
           data = data[0]
       return data['piecePosition']['lane']['endLaneIndex']

    def get_current_lap(self,data):
       if(len(data) == 1):
           data = data[0]
       return data['piecePosition']['lap']


    def speed(self, data):#wyznacza predkosci pojazdu
        global last
        if(last <= self.get_in_piece_Distance(data)): #jezeli od ostatniego ticku nie zmienilismy kawalka
            return self.get_in_piece_Distance(data) - last
        else:
            if( pieces[self.get_piece_index(data)-1].keys().count('angle') == 0 ): #jezeli zmienilismy kawalek a poprzedni nie byl lukiem
                return pieces[self.get_piece_index(data)-1]["length"] - last + self.get_in_piece_Distance(data)
            else:
                angle = math.fabs(pieces[self.get_piece_index(data)-1]["angle"])
                anglesing = math.fabs(pieces[self.get_piece_index(data)-1]["angle"])/pieces[self.get_piece_index(data)-1]["angle"]
                radius = pieces[self.get_piece_index(data)-1]["radius"] - anglesing*lanes[self.get_start_lane_index(data) ]["distanceFromCenter"]
                anglelenght = angle*radius*math.pi/180 #wzor na dlugosc luku
                return anglelenght - last + self.get_in_piece_Distance(data)

    def log_race_data(self,data):
        global f,i
        pieceIndex = self.get_piece_index(data)
        inPieceDistance = self.get_in_piece_Distance(data)
        angle = self.get_car_angle(data)
        f.write(str(i) + "   " + str(pieceIndex) + "   " + str(inPieceDistance) + "   " + str(angle) + '\n')
        i = i + 1

    #end json API

    def __init__(self, socket, name, key):
        global f,i
        self.socket = socket
        self.name = name
        self.key = key
        f = open("Race:" + str(time.time()), 'w')
        i = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        #self.join()
        #self.joinRace()
        self.createRace()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()
##########################################
    def on_createRace(self, data):
        print("createRace")
        self.ping()

    def on_joinRace(self, data):
        print("joinRace")
        self.ping()
    def createRace(self):
        trackName = "germany"
        password = "asd"
        carCount = 1
        return self.msg("createRace", {"botId":{"name": self.name,"key": self.key},"trackName": trackName,"password": password, "carCount": carCount})

    def joinRace(self):
        trackName = "germany"
        password = "asd"
        return self.msg("joinRace", {"botId":{"name": self.name,"key": self.key},"trackName": trackName,"password": password })
###########################
    def on_game_init(self, data):
        global race, track, pieces, lanes
        #data = data[0]
        race = data['race']
        track = race['track']
        pieces = track['pieces']
        lanes = track['lanes']

    def on_game_start(self, data):
        global x, y, tick
        tick = 1
        x = -1
        y = -1
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        global x, y, tick, last
        if self.get_piece_index(data) > 30 and self.get_piece_index(data) < 35:
            self.throttle(1.0)
        elif self.get_piece_index(data) > 35 and self.speed(data) < 0.01:
            print 'liczymy'
            z = np.polyfit(x, y, 4)
            f = np.poly1d(z)
            # calculate new x's and y's
            x_new = np.linspace(x[0], x[-1], 150)
            y_new = f(x_new)
            plt.plot(x,y,'o', x_new, y_new)
            plt.xlim([x[0]-1, x[-1] + 1 ])
            plt.show()
        elif self.get_piece_index(data) > 35:
            print 'apendzimy'
            self.throttle(0)
            if(x == -1):
                x = (tick, )
                y = (self.speed(data), )
            x = x + (tick, )
            y = y + (self.speed(data), )
            tick = tick + 1
        else:
            self.throttle(0.5)
        last = self.get_in_piece_Distance(data)
        self.log_race_data(data)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        global f
        f.close()
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'createRace' : self.on_createRace,
            'joinRace' :self.on_joinRace,
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":


    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
