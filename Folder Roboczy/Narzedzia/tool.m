function tool()
clear ; close all; clc
x = input('Podaj nazwę pliku: ')
data = load(x);

tick = data(:,1);
pieceIndex = data(:,2);
inPieceDistance = data(:,3);
angle = data(:,4);

figure(1);
hold on;
title('Funkcja wychylenia')
xlabel('Ticki')
ylabel('Przebyta Droga')
plot(tick,angle)


%current speed 
current = zeros(size(inPieceDistance));

index = 1;
for i = 1:(size(inPieceDistance,1)-1)
	if (inPieceDistance(i+1) - inPieceDistance(i)) >= 0
		current(index) = inPieceDistance(i+1) - inPieceDistance(i);
		index = index + 1;
	endif
endfor
figure(2)
hold on;
title('Funkcja predkosci')
xlabel('Ticki')
ylabel('Predkosc')
plot(tick,current)


end
