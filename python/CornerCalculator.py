PI = 3.14159


def getCornerLength(alpha, radius):
    alpha = abs(alpha)
    return 2 * PI * radius * alpha / float(360)