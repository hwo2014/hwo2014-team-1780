import math


class JsonAPI(object):
    """
    Usmiechniete API do Jsona.
    RaceData:
    howManyPieces, howManyLines
    """
    global last, last_angle

    def __init__(self, ourCarName):
        self.raceData = None
        self.tickData = None
        self.ourCarName = ourCarName


    def set_race_data(self, raceData):
        """
        Konstruktor przyjmuje dane o trasie, i od razu wrzuca je do odpowiednich zmiennych.
        Dane te dostajemy tylko raz na wyscig, wiec lepiej wyluskac je raz,a nie za kazdym razem
        gdy zajdzie potrzeba skorzystania z nich.
        :param raceData: - Dane o wyscigu
        """
        #poziom 1
        #data ma tylko jeden element - typu 'race' - wiec wypakowywuje
        raceData = raceData['race']
        #poziom 2
        #w race sa trzy pola, 'track', 'cars' i 'raceSession'
        track = raceData['track']
        self.cars = raceData['cars']
        raceSession = raceData['raceSession']
        #poziom 3
        #track
        #  ma 5 pol, 'pieces', 'lanes', 'id', 'startingPoint', 'name'
        #'startingPoint' jest tylko do celow wizualizacyjnych - ignorujemy
        #'id' i 'name' tez nie sa dla nas wazne, ignorujemy
        self.pieces = track['pieces']
        self.lanes = track['lanes']

        #cars
        #  zawiera informacje o wszystkich samochodach na trasie, ma tyle elementow ile jest samodow,
        #kazdy samochod ma swoje 'id' ('color' i 'name') oraz 'dimensions' ('length', 'width', 'guideFlagPosition')
        #narazie tego nie wyluskuje bo nie ma po co

        #raceSession
        # ma 3 pola,'laps' - ilosc okrazen, 'maxLapTimeMs', 'quickRace' - czy musimy odpowiadac na kazdy tick
        #'maxLapTimeMs' nas nie interesuje - ignorujemy
        #ten parametr jest kluczowy w trybie finalowym, najpierw mamy sesje treninogowa, potem wyscig
        # sesja treningowa ma postac "raceSession":{"durationMs":20000}
        #wyscig ma postac "raceSession":{"laps":5, "maxLapTimeMs": 60000, "quickRace":false}
        if raceSession.keys().count('durationMs') == 0:  #wyscig, turniejowy lub nie
            self.qualification = False
            self.quickRace = raceSession['quickRace']
            self.laps = raceSession['laps']
        else:
            self.qualification = True
            self.qualificationDuration = raceSession['durationMs']


            #poziom 4
            #pieces
            # kazdy z pieces moze miec 1 lub 2 parametry
            #prosta ma parametr 'length' okreslajacy dlugosc i opcjonalnie 'switch'
        #zakret ma parametry 'angle' i 'radius'

    def current_speed(self):
        #od statniego ticku nie zmienilismy kawalka
        if self.last <= self.in_piece_distance():
            ret = self.in_piece_distance() - self.last
        #zmienilismy kawalek od ostatniego ticku
        else:
            #poprzedni nie byl lukiem
            if self.is_this_piece_corner(self.piece_index() - 1) is not True:
                ret = self.pieces[self.piece_index() - 1]["length"] - self.last + self.in_piece_distance()
            #poprzedni byl lukiem
            else:
                angle = math.fabs(self.pieces[self.piece_index() - 1]["angle"])
                anglesing = math.fabs(self.pieces[self.piece_index() - 1]["angle"]) / \
                            self.pieces[self.piece_index() - 1]["angle"]
                radius = self.pieces[self.piece_index() - 1]["radius"] - anglesing * self.distance_from_center(
                    self.start_lane_index())
                anglelenght = angle * radius * math.pi / 180  #wzor na dlugosc luku
                ret = anglelenght - self.last + self.in_piece_distance()
        #sel    f.last = self.in_piece_distance()
        return ret

    def car_angle_speed(self):
        return self.car_angle() - self.last_angle

    def piece_length(self, i):
        if self.is_this_piece_corner(i) is False:
            return self.get_piece(i)['length']
        else:
            angle = math.fabs(self.get_piece(i)["angle"])
            radius = self.angle_radius(i)
            anglelenght = angle * radius * math.pi / 180  #wzor na dlugosc luku
            return anglelenght

    def angle_radius(self, i):
        anglesing = (math.fabs(self.get_piece(i)["angle"])) / self.get_piece(i)["angle"]
        return self.get_piece(i)["radius"] - anglesing * self.distance_from_center(self.end_lane_index())


    def get_distance_from_center(self, laneIndex):
        return self.lanes[laneIndex]['distanceFromCenter']

    def next_angle_radius(self):
        i = self.piece_index()
        while (1 == 1):
            if ( self.pieces[i].keys().count('radius') != 0 ):
                anglesing = math.fabs(self.pieces[i]["angle"]) / self.pieces[i]["angle"]
                return self.pieces[i]["radius"] - anglesing * self.lanes[self.start_lane_index()]["distanceFromCenter"]
            i += 1
            if (i == len(self.pieces)):  #gdy dojdziemy do konca okrazenia to wracamy do i=0
                i = 0

    def next_angle_type(self):
        i = self.piece_index()
        while (1 == 1):
            if ( self.pieces[i].keys().count('radius') != 0 ):
                return math.fabs(self.pieces[i]["angle"]) / self.pieces[i]["angle"]
            i += 1
            if (i == len(self.pieces)):  #gdy dojdziemy do konca okrazenia to wracamy do i=0
                i = 0

    def how_many_corners(self):
        corners = 0
        pieces = self.how_many_pieces()
        for i in range(1, pieces):
            if self.is_this_piece_corner(i) is True:
                corners += 1
        return corners

    def how_many_pieces(self):
        return len(self.pieces)

    def qualification_duration(self):
        return self.qualificationDuration

    def is_this_qualification(self):
        return self.qualification

    def pieces(self):
        return self.pieces

    def is_this_piece_corner(self, pieceIndex):
        if self.pieces[pieceIndex].keys().count('radius') == 0:
            return False
        return True

    def get_piece_radius(self, pieceIndex):
        if 'radius' in self.pieces[pieceIndex]:
            return self.pieces[pieceIndex]['radius']
        else:
            return 0

    def have_this_piece_switch(self, pieceIndex):
        if self.pieces[pieceIndex]['switch'] is not None:
            return self.pieces[pieceIndex]['switch']
        else:
            return False

    def get_piece(self, pieceIndex):
        return self.pieces[pieceIndex]

    def how_many_lanes(self):
        return len(self.lanes)

    #lanes sa numerowane od zera !!
    def distance_from_center(self, laneIndex):
        return self.lanes[laneIndex]['distanceFromCenter']

    def how_many_cars(self):
        return len(self.cars)

    def set_tick_data(self, data, l, la):
        """
            Ta funkcja powinna byc wywolywana w kazdym ticku, odswierzamy dane do wyluskiwania pol
            :param data:
         """
        #narazie interesuje nas tylko nasz samochod, to trzeba bedzie zmienic
        global last, last_angle
        self.last = l
        self.last_angle = la

        for car in data:
            if car["id"]["name"] == self.ourCarName:
                ourCarData = car
                break
        self.tickData = ourCarData

    def way_to_angle(self):  # wyznacza odleglosc do najblizszego zakretu
        global race, track, pieces
        i = self.piece_index()  # zmienna przechodzaca po kolejnych kawalkch
        if self.is_this_piece_corner(i) is not True:
            way = self.get_piece(i)["length"] - self.in_piece_distance()  # odleglosc do konca aktualnego kawalka
            while True:
                i += 1
                if (i == self.how_many_pieces()):  #gdy dojdziemy do konca okrazenia to wracamy do i=0
                    i = 0
                if self.is_this_piece_corner(i) is True:
                    return way
                way += self.get_piece(i)["length"]
        else:  # jezeli jestesmy na zakrecie
            return 0.0

    def piece_index(self):
        return self.tickData['piecePosition']['pieceIndex']

    def in_piece_distance(self):
        return self.tickData['piecePosition']['inPieceDistance']

    def car_angle(self):
        return self.tickData['angle']

    def start_lane_index(self):
        return self.tickData['piecePosition']['lane']['startLaneIndex']

    def end_lane_index(self):
        return self.tickData['piecePosition']['lane']['endLaneIndex']

    def current_lap(self):
        return self.tickData['piecePosition']['lap']