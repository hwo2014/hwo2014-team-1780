class RoadPlan():
    def __init__(self):
        self.roads = dict()

    def addSinglePath(self, start, end, road, length):
        if start not in self.roads:
            self.roads[start] = dict()
        self.roads[start][end] = {
            'path': road,
            'length': length
        }

    def getBestLine(self):
        bestStart = 0
        bestEnd = 0
        bestLen = 999999999
        for start in self.roads:
            for end in self.roads[start]:
                if bestLen > self.roads[start][end]['length']:
                    bestLen = self.roads[start][end]['length']
                    bestStart = start
                    bestEnd = end
        return {
            'from': bestStart,
            'to': bestEnd,
            'path': self.roads[bestStart][bestEnd]['path'],
            'length': self.roads[bestStart][bestEnd]['length']
        }


    def __str__(self):
        result = ""
        for start in self.roads:
            for end in self.roads[start]:
                result += "%d -> %d\n" % (start, end)
                result += "\tlength: %.2f\n" % (self.roads[start][end]['length'])
                result += "\troad:\n"
                for pieceIndex in sorted(self.roads[start][end]['path']):
                    result += "\t\tna switchu %d wyjezdzamy %d\n" % (
                    pieceIndex, self.roads[start][end]['path'][pieceIndex] )
        return result