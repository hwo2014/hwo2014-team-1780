from Queue import PriorityQueue
import copy
import threading
from CornerCalculator import getCornerLength
from RoadPlan import RoadPlan


class PathArrayEntry:
    def __init__(self, nextPartLength):
        self.nextPartLength = nextPartLength
        self.totalLength = 999999999
        self.previous = -1

    def __str__(self):
        return "next: %.2f, total: %.2f, previous: %d" % (self.nextPartLength, self.totalLength, self.previous)


class RoadPlanner(threading.Thread):
    SWITCH_KEY = 'switch'
    RADIUS_KEY = 'radius'

    def __init__(self, pieces, lanes, successHandler):
        threading.Thread.__init__(self)
        self.pieces = pieces
        self.lanes = lanes
        self.numOfLanes = len(lanes)
        self.successHandler = successHandler
        self.switchIndexes = list()

    def insertRowIntoPathArray(self, arrayEntries, pathArray):
        pathArray.append(arrayEntries)

    def preparePathArray(self):
        arrayEntries = dict()
        pathArray = []
        isLastSwitch = False
        for pieceIndex, piece in enumerate(self.pieces):
            for lane in self.lanes:
                distFromCenter = float(lane["distanceFromCenter"])
                index = int(lane["index"])
                if self.isCorner(piece):
                    angle = float(piece["angle"])
                    laneCorrection = distFromCenter if angle < 0 else -distFromCenter
                    radius = float(piece["radius"]) + laneCorrection
                    valueToAdd = getCornerLength(angle, radius)
                else:
                    valueToAdd = float(piece["length"])
                if index not in arrayEntries:
                    arrayEntries[index] = PathArrayEntry(0)
                arrayEntries[index].nextPartLength += valueToAdd
            isLastSwitch = False
            if self.hasSwitch(piece):
                self.insertRowIntoPathArray(arrayEntries, pathArray)
                self.switchIndexes.append(pieceIndex)
                arrayEntries = dict()
                isLastSwitch = True
        if isLastSwitch is False:
            self.insertRowIntoPathArray(arrayEntries, pathArray)
        else:
            fakeArrayEntries = dict()
            for lane in self.lanes:
                fakeArrayEntries[int(lane["index"])] = PathArrayEntry(0)
            self.insertRowIntoPathArray(fakeArrayEntries, pathArray)
        return pathArray

    def findPath(self, pathArray, startLaneIndex):
        class QueueEntry:
            def __init__(self, row, laneIndex, totalLength):
                self.row = row
                self.laneIndex = laneIndex
                self.totalLength = totalLength

            def __cmp__(self, other):
                return cmp(self.totalLength, other.totalLength)

            def __str__(self):
                return "row: %d, laneIndex: %d, totalLength: %.2f" % (self.row, self.laneIndex, self.totalLength)

        queue = PriorityQueue()
        queue.put(QueueEntry(0, startLaneIndex, 0))
        while not queue.empty():
            entry = queue.get()
            if entry.row >= len(pathArray):
                break
            pathArrayRow = pathArray[entry.row]
            current = pathArrayRow[entry.laneIndex]
            nextPathArrayRow = pathArray[(entry.row + 1) % len(pathArray)]
            laneIndexes = [entry.laneIndex - 1, entry.laneIndex, entry.laneIndex + 1]
            for laneIndex in laneIndexes:
                if laneIndex not in nextPathArrayRow:
                    continue
                next = nextPathArrayRow[laneIndex]
                if next.totalLength > current.nextPartLength + entry.totalLength:
                    next.totalLength = current.nextPartLength + entry.totalLength
                    next.previous = entry.laneIndex
                    queue.put(QueueEntry(entry.row + 1, laneIndex, next.totalLength))

    def run(self):
        print "Szukam najlepszej trasy wsrod {} pieces".format(len(self.pieces))
        pathArray = self.preparePathArray()
        roadPlan = RoadPlan()
        for i in range(self.numOfLanes):
            startLane = i
            pathArrayCopy = copy.deepcopy(pathArray)
            self.findPath(pathArrayCopy, startLane)
            #self.printPathArray(pathArrayCopy)
            for lane in self.lanes:
                index = int(lane["index"])
                length = pathArrayCopy[0][index].nextPartLength
                prev = index
                road = dict()  # slownik, klucz to pieceIndex (w ktorym jest switch), a wartosc to indexLane ktorym trzeba wyjechac z tego switcha
                rowNum = len(pathArray) - 1
                switchIndexesIndex = len(self.switchIndexes) - 1
                while rowNum > 0:
                    length += pathArrayCopy[rowNum][prev].nextPartLength
                    road[self.switchIndexes[switchIndexesIndex]] = prev
                    prev = pathArrayCopy[rowNum][prev].previous
                    switchIndexesIndex -= 1
                    rowNum -= 1
                roadPlan.addSinglePath(i, index, road, length)
        self.successHandler(roadPlan)

    def printPathArray(self, pathArray):
        for row in pathArray:
            for index, entry in row.items():
                print "{} => {}".format(index, entry)

    def isCorner(self, piece):
        return self.RADIUS_KEY in piece

    def hasSwitch(self, piece):
        return self.SWITCH_KEY in piece