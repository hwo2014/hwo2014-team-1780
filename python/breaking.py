def help_breaking(self, current, planned):
    '''
    Ta funkcja to nic innego jak 2 x wyszukiwanie binarne, na funkcji hamowania, a na koncu petla zliczajaca predkosci
    posrednie miedzy tickami, czyli to co nas interesuje.
    '''
    left = -50
    right = 300
    while True:
        center = (left + right) // 2
        x = self.breaking_function(center)
        #    print abs(x), abs(current)
        if center == left or center == right:
            current_tick = center
            break
        if x > current:
            left = center + 1
        else:
            right = center - 1

    left = -50
    right = 300
    while True:
        center = (left + right) // 2
        x = self.breaking_function(center)
        if center == left or center == right:
            planned_tick = center
            break
        if x > planned:
            left = center + 1
        else:
            right = center - 1
    sum = 0.0
    #print 'Przedzialy: ', current_tick, planned_tick
    while current_tick < planned_tick:
        sum = sum + self.breaking_function(current_tick)
        current_tick = current_tick + 1
        # print 'Suma: ', sum
    return sum


def breaking(self, current, target):
    '''
    Ta funkcja szacuje nam ile metrow potrzebujemy by wychamowac z danej predkosci current,
    do predkosci target. Funkcja jest napisana tak czytelnie ze Donald Knuth jakby ja zobaczyl
    to chcialby mnie usynowic za moje mistrzostwo w literate programmingu. Mimo to ta funkcja dziala zaskakujaco
    dobrze. Robilem eksperymenty i wychamowywujac z wartosci speed 5 do 3 mialem blad okolo 1 jednostki!
    Przy wychamowaniu z 9 do 2 to bylo 18 jednostek.
    Wynik mozna poprawic operujac parametrem_bezpiecznosci. Gdy sie go podniesie do okolicy 1 albo troche
    wiecej niz 1 to powinien dawac mniejsze bledy, ale niestety ma tendencje do oszacowan zbyt optymistycznych.
    Wartosc 0.9 przybliza gorzej, ale zawsze od dolu wiec dla nas bezpieczniej. nie mniej jednak mozna pomyslec o
    zmianie tego parametru.
    '''
    alfa = 1.0
    beta = (current / 6.65132996166)
    a = (alfa - beta)/200
    b = (alfa + 3*beta)/4
    #a = a * 0.97124
    #b = b * 0.99678543
    base_function_coeffs = [-2.04976379e-11, 1.74428809e-08, -6.25357501e-06, 1.22046776e-03, -1.34461356e-01, 6.87558173]

    # ret = [0] * len(speed)
    # for i in range(len(speed)):
    #     ret[i] = a * speed[i] + b
    # for i in range(len(speed)):
    #     speed[i] = speed[i] * ret[i]
    #
    # z = np.polyfit(ticki, speed, 5)
    # self.breaking_function = np.poly1d(z)

    # x_new = np.linspace(-50, 200, 350)
    # y_new = breaking_function(x_new)
    # plt.plot(ticki,speed,'o', x_new, y_new)
    # plt.xlim([-50, 200 + 1 ])
    # plt.show()

    A = base_function_coeffs[0]
    B = base_function_coeffs[1]
    C = base_function_coeffs[2]
    D = base_function_coeffs[3]
    E = base_function_coeffs[4]
    F = base_function_coeffs[5]

    newCoeffs = [0] * 6
    newCoeffs[0] = -0.000000000021000 #duze bledy zaokraglen a zmienia sie bardzo malo wiec lepiej wpisac z palca
    # newCoeffs[0] = 2 * a * (A * F + B * E + C * D) + b * A
    newCoeffs[1] = a * (2 * B * F + 2 * C * E + D * D) + b * B
    newCoeffs[2] = 2 * a * (C * F + D * E) + b * C
    newCoeffs[3] = a * (2 * D * F + E * E) + b * D
    newCoeffs[4] = 2 * a * E * F + b * E
    newCoeffs[5] = a * F * F + b * F

    # for i in range(6):
    #     print "target: %.15f, current: %.15f" % (self.breaking_function.coeffs[i], newCoeffs[i])
    # print "-----------"

    self.breaking_function = lambda x: sum([newCoeffs[i] * x ** (5-i) for i in range(6)])
    return self.help_breaking(current, target)