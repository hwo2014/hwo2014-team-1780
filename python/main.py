import json
import socket
import sys
import time
#import matplotlib.pyplot as plt
import math
from RoadPlanner import RoadPlanner
#from RoadPlan import RoadPlan
from JsonAPI import JsonAPI

ON_SERVER = True # jak wrzucamy na serwer to to ma byc True (zeby nie tworzylo pliku z logiem)

class NoobBot(object):

    #json API
    #turbo_on 0 - nie dostepne, 1 - dostepne, uzywane, 2 - dostepne
    global race, track, pieces, lanes, last, last_angle, raceSession, turbo_on
    from breaking import *

    def get_piece_index(self, ourCar):
        return ourCar['piecePosition']['pieceIndex']

    def get_in_piece_Distance(self, ourCar):
        return ourCar['piecePosition']['inPieceDistance']

    def get_car_angle(self, ourCar):
        return ourCar['angle']

    def get_start_lane_index(self, ourCar):
        return ourCar['piecePosition']['lane']['startLaneIndex']

    def get_end_lane_index(self, ourCar):
        return ourCar['piecePosition']['lane']['endLaneIndex']

    def get_distance_from_center(self, laneIndex):
        global lanes
        return lanes[laneIndex]['distanceFromCenter']

    def get_current_lap(self, ourCar):
        return ourCar['piecePosition']['lap']

    def max_speed(self, data):
        return self.max_angle_function(self.api.next_angle_radius())

    def max_angle_function(self, x):
        return 0.0305951* x+3.69777#+0.21
        #return 0.000000137377*x*x*x-0.0000510311*x*x+0.0360417*x+3.53697

    def piece_length(self, i):
        global pieces
        if(pieces[i].keys().count('length') != 0):
            return pieces[i]['length']
        else:
            angle = math.fabs(pieces[i]["angle"])
            radius = self.angle_radius(data, i)
            anglelenght = angle * radius * math.pi / 180  #wzor na dlugosc luku
            return anglelenght

    def angle_radius(self,data, i):
        global pieces
        anglesing = (math.fabs(pieces[i]["angle"])) / pieces[i]["angle"]
        return pieces[i]["radius"] - anglesing * self.get_distance_from_center(self.which_lane(data,i))

    def which_lane(self,data, i):
        while True:
            if(i == self.get_piece_index(data)):
                return self.get_end_lane_index(data)
            if (i in self.bestLine['path']):
                return self.bestLine['path'][i]
            i -= 1
            if(i < 0):
                i = len(self.pieces)

    def log_race_data(self,ourCar):
        global f,i
        pieceIndex = self.get_piece_index(ourCar)
        inPieceDistance = self.get_in_piece_Distance(ourCar)
        angle = self.get_car_angle(ourCar)
        v = self.api.current_speed()
        if not ON_SERVER:
            f.write(str(i) + "   " + str(pieceIndex) + "   " + str(inPieceDistance) + "   " + str(angle) + "  " + str(
                v) + " " + str(math.fabs(self.api.car_angle_speed())) + '\n')
            i = i + 1

    #end json API

    def __init__(self, socket, name, key):
        global f,i
        self.best_line_in_every_piece = []
        self.socket = socket
        self.name = name
        self.key = key
        self.currentTick = 0
        self.breaking_function = None
        self.max_speed_list = []
        self.currentThrotte = 0.0
        if not ON_SERVER:
            f = open("Race" + str(time.time()), 'w')
        print "Race" + str(time.time())
        i = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.currentThrotte = throttle
        self.msg("throttle", throttle)

    def turbo(self):
        self.msg("turbo", "I see fire, inside the engine...")

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        #self.joinRace()
        # self.createRace()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()
##########################################
    def on_createRace(self, data):
        print("createRace")
        self.ping()

    def on_joinRace(self, data):
        print("joinRace")
        self.ping()

    def createRace(self):
        trackName = "england" #keimola, germany, usa, france, elaeintarha, imola, england, suzuka
        password = "asd"
        carCount = 1
        return self.msg("createRace", {"botId":{"name": self.name,"key": self.key},"trackName": trackName,"password": password, "carCount": carCount})

    def joinRace(self):
        trackName = "keimola"
        password = "asd"
        return self.msg("joinRace", {"botId":{"name": self.name,"key": self.key},"trackName": trackName,"password": password })
###########################
    def on_game_init(self, data):
        self.api.set_race_data(data)
        global race, track, pieces, lanes, raceSession
        if not ON_SERVER:
            f.write(str(data))
        #data = data[0]
        race = data['race']
        track = race['track']
        pieces = track['pieces']
        lanes = track['lanes']
        raceSession = race['raceSession']
        roadPlanner = RoadPlanner(pieces, lanes, self.on_road_found)
        roadPlanner.start()

    def max_speed_planner(self):
        for pieceIndex in range(self.api.how_many_pieces()):
            if self.api.is_this_piece_corner(pieceIndex) is False:
                self.max_speed_list.append(11)
            else:
                piece_radius = self.api.get_piece_radius(pieceIndex)
                anglesing = (math.fabs(self.api.get_piece(pieceIndex)["angle"])) / self.api.get_piece(pieceIndex)["angle"]
                piece_radius -= anglesing * self.api.distance_from_center(self.best_line_in_every_piece[pieceIndex])
                #print piece_radius
                self.max_speed_list.append(self.max_angle_function(piece_radius))

        max_speed_list_copy =  list(self.max_speed_list)
        for pieceIndex in range(len(self.max_speed_list) - 1):  # dla kazdego kawalka poza ostatnim zeby nie przekraczac indexow
            if self.max_speed_list[pieceIndex] > self.max_speed_list[pieceIndex + 1]: # grozne przypadki to te ktore sa spadkami
                breaking_distance = self.breaking(self.max_speed_list[pieceIndex], self.max_speed_list[pieceIndex + 1])
                pieceIndexCopy = pieceIndex
                while breaking_distance > self.api.piece_length(pieceIndexCopy): # jesli sciezka hamowania dluzsza niz current piece
                    if abs(self.api.piece_length(pieceIndexCopy) - self.api.piece_length(pieceIndexCopy + 1)) < 0.5: #jesli kawalki nie roznia sie bardzo
                        self.max_speed_list[pieceIndexCopy] /=1.29 # tym manipulujemy
                    else:
                        self.max_speed_list[pieceIndexCopy] = self.max_speed_list[pieceIndexCopy + 1] * 1.29 # tym tez
                    breaking_distance -= self.api.piece_length(pieceIndexCopy)
                    pieceIndexCopy -=1
        for index in range(len(self.max_speed_list)):
            self.max_speed_list[index] = min(self.max_speed_list[index], max_speed_list_copy[index])
            #print self.max_speed_list[index], max_speed_list_copy[index]


    def on_your_car(self,data):
        self.ourCarName = data["name"]
        self.api = JsonAPI(self.ourCarName)

    def on_road_found(self, roadPlan):
        print "ROAD FOUND, tick %d" % (self.currentTick,)
        print "ROADPLAN:\n%s" % (roadPlan,)
        print "BESTLINE:\n%s" % (roadPlan.getBestLine(),)
        self.roadPlan = roadPlan
        self.bestLine = roadPlan.getBestLine()
        self.best_line_in_every_piece.append(self.bestLine['from'])
        for pieceIndex in range(self.api.how_many_pieces()):
            if pieceIndex in self.bestLine['path']:
                self.best_line_in_every_piece.append(self.bestLine['path'][pieceIndex])
            else:
                self.best_line_in_every_piece.append(self.best_line_in_every_piece[-1])
        self.best_line_in_every_piece.pop(0)

        self.max_speed_planner()

    def on_game_start(self, data):
        global x, y, tick
        tick = 1
        x = -1
        y = -1
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        global tick, last, last_angle, pieces, turbo_on, track
        self.api.set_tick_data(data, last, last_angle)
        for car in data:
            if car["id"]["name"] == self.ourCarName:
                ourCar = car
                break
        #zmiana torow

        if(len(pieces)-1 == self.get_piece_index(ourCar)):
            next_piece = 0
        else:
            next_piece = self.get_piece_index(ourCar)+1
        if(last> self.get_in_piece_Distance(ourCar)):
            if(pieces[next_piece].keys().count('switch') != 0):
                end_lane = self.bestLine['path'][next_piece]
                if(end_lane < self.get_end_lane_index(ourCar)):
                    self.msg("switchLane", "Left")
                elif (end_lane > self.get_end_lane_index(ourCar)):
                    self.msg("switchLane", "Right")

        #niwelowanie wychylen
        if(self.api.next_angle_type()*self.api.car_angle_speed()<= 0 ):
            car_angle_punish = 0
        else:
            car_angle_punish = math.fabs(self.api.car_angle_speed())-1
            if(car_angle_punish<0):
                car_angle_punish=0
        #turbo
        turbo_start = -1
        if(track['id']=="usa"):
            car_angle_punish = 0
            turbo_start=12
        elif(track['id']=="keimola"):
            turbo_start=35
        elif(track['id']=="germany"):
            turbo_start=40
        elif(track['id']=="france"):
            turbo_start=37

        #ustalanie predkosci

        if("laps" in raceSession and self.get_current_lap(ourCar)== raceSession["laps"]-1 and (self.get_piece_index(ourCar)==turbo_start or(turbo_start==-1 and self.api.way_to_angle()>500))): #usa 11 bez, keimola 35 z 2, germany 39 z 1
            self.turbo()
        elif(self.api.current_speed() ==0):
            self.throttle(1)
        elif(self.api.current_speed() > self.max_speed_list[self.get_piece_index(ourCar)]): #jezeli jedziemy szybciej niz mozemy w danym kawalku
            self.throttle(0)
        elif(self.api.piece_length(self.get_piece_index(ourCar))-self.get_in_piece_Distance(ourCar)< self.breaking(self.api.current_speed(), self.max_speed_list[next_piece]-car_angle_punish)): #jezeli musimy zaczac hamowac przed nastepnym kawalkiem
            self.throttle(0)
        else:
            self.throttle(1)

        #elif(self.api.way_to_angle() > self.breaking(self.api.current_speed(),self.max_speed(data)-car_angle_punish)):
       #     self.throttle(1)
       # else:
        #    if(self.max_speed(data)-car_angle_punish<self.api.current_speed() ):
        #        self.throttle(0)
         #   else:
         #       self.throttle(1)

        if ON_SERVER:
            self.log_race_data(ourCar)
        last = self.get_in_piece_Distance(ourCar)
        last_angle = self.get_car_angle(ourCar)
        #print self.currentTick, self.max_speed_list[self.api.piece_index()], self.api.current_speed(), self.currentThrotte

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        global f
        if not ON_SERVER:
            f.close()
        self.ping()

    def on_turboAvailable(self, data):
        global turbo_on
        turbo_on = 2
        print ("turboAvailable")
        #print data
        self.ping()

    def on_turboStart(self, data):
        global turbo_on
        turbo_on = 1
        print ("I've got the power!!!")

        self.ping()

    def on_turboEnd(self, data):
        global turbo_on
        print ("I've no power ;(")
        turbo_on=0
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        global last, last_angle, turbo_on
        last=0
        last_angle=0
        turbo_on = 0

        msg_map = {
            'createRace' : self.on_createRace,
            'joinRace' :self.on_joinRace,
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'yourCar': self.on_your_car,
            'turboAvailable': self.on_turboAvailable,
            'turboStart': self.on_turboStart,
            'turboEnd': self.on_turboEnd,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        printed = False
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.currentTick = int(msg['gameTick'])
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":

    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()